package com.qqs.qqsoft.db.user;


public interface User {
    int getId();
    String getFirstName();
    String getLastName();
    String getUserName();
    Boolean getActive();
    String getRoles();
    Integer getDeskId();
}
