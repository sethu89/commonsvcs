package com.qqs.qqsoft.db.user;

import com.qqs.qqsoft.db.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Component
public class UserDataService {
    @Autowired
    private UserRepository userRepository;

    @Resource
    private EntityManager entityManager;

    public Optional<UserEntity> getMassageById(Integer id) {
        return userRepository.findById(id);
    }

    public Optional<List<UserEntity>> getUserByName(String firstName, String lastName) {
        return userRepository.findUserEntitiesByName(firstName, lastName);
    }

    public Optional<UserEntity> getUserByUserName(String userName) {
        return userRepository.findUserEntitiesByUserName(userName);
    }

    public Optional<List<Object[]>> getAllUserName() {
        return userRepository.findAllUsers();
    }

    public Optional<UserEntity> getUserById(Integer id) {
        return userRepository.findById(id);
    }

    public Optional<User> getUserByuserName(String userName) {
        return userRepository.getUserByuserName(userName);
    }

    public Optional<List<UserEntity>> findAllByUserNameIsLike(String userName) {
        return userRepository.findAllByUserNameIsLike(userName);
    }

    public Iterable<UserEntity> findAll() {
        return userRepository.findAll();
    }

    @Transactional
    public UserEntity saveUser(UserEntity item) {
        return userRepository.save(item);
    }
}
