package com.qqs.qqsoft.db.repository;

import com.qqs.qqsoft.db.user.User;
import com.qqs.qqsoft.db.user.UserEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends CrudRepository<UserEntity, Integer> {

    @Query(value = "select * from user where firstName like ?1 AND lastName like ?2 AND active = true", nativeQuery = true)
    Optional<List<UserEntity>> findUserEntitiesByName(String firstName, String LastName);

    @Query(value = "select * from user where userName = ?1 AND active = true", nativeQuery = true)
    Optional<UserEntity> findUserEntitiesByUserName(String userName);

    @Query(value = "select id, concat(firstName, ' ', lastName) from user where active = true", nativeQuery = true)
    Optional<List<Object[]>> findAllUsers();

    @Query(value = "select id, firstName, lastName, userName, active, roles from user where userName = ?1 AND active = true", nativeQuery = true)
    Optional<User> getUserByuserName(String userName);

    Optional<List<UserEntity>> findAllByUserNameIsLike(String userName);

}
