package com.qqs.qqsoft;

public class QQBusinessException extends Exception{
    public QQBusinessException(String message) {
        super(message);
    }

    public QQBusinessException(String message, Throwable cause) {
        super(message, cause);
    }
}
