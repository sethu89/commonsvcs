package com.qqs.qqsoft.utils;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class PasswordUtils {
    /**
     * Password encryptor for BCrypt passwords
     * @param args
     */
    public static void main(String[] args) {
        try {
            String rawPass = "fte123";
            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(12);
            String password = encoder.encode(rawPass);
            System.out.println(password);
            System.out.println("Match : " + encoder.matches(rawPass, password));
            System.out.println("Done");
        } catch (Exception e){
            System.out.println("Exception");
        }
    }
}
