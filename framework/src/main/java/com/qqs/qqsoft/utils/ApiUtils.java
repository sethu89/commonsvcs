package com.qqs.qqsoft.utils;

import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ApiUtils<S, T> {
    public T translate(S source, Class<T> target, boolean excludeStamp) throws InstantiationException, IllegalAccessException {
        T t = translate(source, target, excludeStamp, new String[]{});
        return t;
    }

    public List<T> translate(Iterable<S> source, Class<T> target, boolean excludeStamp) throws IllegalAccessException, InstantiationException {
        List<T> result = new ArrayList<>();
        if(source == null) return result;
        for (S s : source) {
            T t = translate(s, target, excludeStamp, new String[]{});
            result.add(t);
        }
        return result;
    }

    public T translate(S source, Class<T> target, boolean excludeStamp, String... params) throws InstantiationException, IllegalAccessException {
        T t = createTarget(target);
        translate(source, t, excludeStamp, params);
        return t;
    }

    public void translate(S source, T target, String... params) {
        translate(source, target, true);
    }

    public void translate(S source, T target, boolean excludeStamp, String... params) {
        String[] toParams = new String[0];
        if (excludeStamp) {
            toParams = new String[]{"createdDt", "modifiedDt", "createdBy", "modifiedBy"};
        }
        String[] ignoreParams = Arrays.copyOf(toParams, toParams.length + params.length);
        for (int i = 0; i < params.length; i++) {
            ignoreParams[toParams.length + i] = params[i];
        }
        BeanUtils.copyProperties(source, target, ignoreParams);
        return;
    }

    private T createTarget(Class<T> target) throws IllegalAccessException, InstantiationException {
        return target.newInstance();
    }
}
