package com.qqs.qqsoft.utils;

import com.qqs.qqsoft.QQBusinessException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Component
public class SearchCriteriaUtils {

    public void validateCriteria(Map<String, String> searchParams, Set<String> validColumns) throws QQBusinessException {
        for (String p : searchParams.keySet()) {
            if (!validColumns.contains(p)) {
                throw new QQBusinessException("Search criteria invalid");
            }
        }
    }

    public List<SearchCriteria> createSearchCriteria(Map<String, String> params,
                                                     Boolean exactMatch, Map<String, String> operationMap,
                                                     Set<String> validColumns) throws QQBusinessException {
        List<SearchCriteria> conditions = new ArrayList<>(params.size());
        validateCriteria(params, validColumns);
        for (String p : params.keySet()) {
            SearchCriteria criteria = new SearchCriteria();
            if(operationMap == null || operationMap.get(p) == null) {
                criteria.setOperation(exactMatch ? "=" : ":");
            } else {
                criteria.setOperation(validateOperation(operationMap.get(p)));
            }
            criteria.setKey(p);
            criteria.setValue(params.get(p));
            conditions.add(criteria);
        }
        return conditions;
    }

    private String validateOperation(String op){
        if(op.matches("[><=:^]")){
            return op;
        }
        return "=";
    }
}
