package com.qqs.qqsoft.utils;

import com.qqs.qqsoft.service.model.QQUserDetails;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class SecurityUtils {
    public Authentication getAuthentication(){
        return SecurityContextHolder.getContext().getAuthentication();
    }

    public Integer getLoggedInUser(){
        return getLoggedInUserComplete().getDbUserId();
    }

    public String getLoggedInUserName(){
        QQUserDetails user = getLoggedInUserComplete();
        return user.getLastName() + ", " + user.getFirstName();
    }

    public QQUserDetails getLoggedInUserComplete(){
        Authentication auth = this.getAuthentication();
        QQUserDetails user = (QQUserDetails) auth.getPrincipal();
        return user;
    }

    public boolean isUserHasElevatedAccess(){
        QQUserDetails user = this.getLoggedInUserComplete();
        for(GrantedAuthority g : user.getAuthorities()){
            if(g.getAuthority().equalsIgnoreCase("ROLE_ADMIN")){
                return true;
            }
        }
        return false;
    }
}
