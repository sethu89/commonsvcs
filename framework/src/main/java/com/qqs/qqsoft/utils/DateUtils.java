package com.qqs.qqsoft.utils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Timestamp;

public class DateUtils<T> {
    public static Timestamp getCurrentTime() {
        return new Timestamp(System.currentTimeMillis());
    }

    public void setTimeStamp(T obj, Class<T> clazz) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        setTimeStamp(obj, clazz, false);
    }

    public void setModifiedTimeStamp(T obj, Class<T> clazz) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method m = clazz.getMethod("setModifiedDt", Timestamp.class);
        m.invoke(obj, getCurrentTime());
    }

    public void setTimeStamp(T obj, Class<T> clazz, boolean update) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        if(!update) {
            Method m = clazz.getMethod("setCreatedDt", Timestamp.class);
            m.invoke(obj, getCurrentTime());
        }
        setModifiedTimeStamp(obj, clazz);
    }


}
