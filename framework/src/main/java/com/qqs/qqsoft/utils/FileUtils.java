package com.qqs.qqsoft.utils;

import org.springframework.web.multipart.MultipartFile;

import java.io.*;

public class FileUtils {

    public String uploadFile(MultipartFile file, String fileType, String uploadFolder) {
        String filename = file.getOriginalFilename();
        System.out.println(uploadFolder+" "+filename);
        try {
            byte[] bytes = file.getBytes();
            BufferedOutputStream stream = null;
            stream = new BufferedOutputStream(new FileOutputStream(
                    new File(uploadFolder + File.separator + filename)));
            stream.write(bytes);
            stream.flush();
            stream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return "Error";
        } catch (IOException e) {
            e.printStackTrace();
            return "Error";
        }

        return "Success";
    }
}
