package com.qqs.qqsoft.utils;

import java.util.*;

public class CurrencyUtils {
    private static final Map<Integer, String> metricPosition = new HashMap<>();
    private static final Map<Integer, String> tees = new HashMap<>();
    private static final Map<Integer, String> teens = new HashMap<>();
    private static final Map<Integer, String> ones = new HashMap<>();
    private static final int dec = 10;
    public static final Map<String, Map<String, String>> currencyMap = new HashMap<>();


    static {
        Map<String, String> currencyAttr = new HashMap();
        currencyAttr.put("decimalText","dollars");
        currencyAttr.put("fractionText","cents");
        currencyMap.put("USD", currencyAttr);
        currencyAttr = new HashMap();
        currencyAttr.put("decimalText","euro");
        currencyAttr.put("fractionText","cents");
        currencyMap.put("EUR", currencyAttr);
        currencyAttr = new HashMap();
        currencyAttr.put("decimalText","rupees");
        currencyAttr.put("fractionText","paise");
        currencyMap.put("INR", currencyAttr);
    }

    static {
        metricPosition.put(1, "Ten");
        metricPosition.put(2, "Hundred");
        metricPosition.put(3, "Thousand");
        metricPosition.put(5, "Lakh");
        metricPosition.put(7, "Crore");
    }

    static {
        tees.put(2, "Twenty");
        tees.put(3, "Thirty");
        tees.put(4, "Forty");
        tees.put(5, "Fifty");
        tees.put(6, "Sixty");
        tees.put(7, "Seventy");
        tees.put(8, "Eighty");
        tees.put(9, "Ninety");
    }

    static {
        teens.put(10, "Ten");
        teens.put(11, "Eleven");
        teens.put(12, "Twelve");
        teens.put(13, "Thirteen");
        teens.put(14, "Fourteen");
        teens.put(15, "Fifteen");
        teens.put(16, "Sixteen");
        teens.put(17, "Seventeen");
        teens.put(18, "Eighteen");
        teens.put(19, "Nineteen");
    }

    static {
        ones.put(1, "One");
        ones.put(2, "Two");
        ones.put(3, "Three");
        ones.put(4, "Four");
        ones.put(5, "Five");
        ones.put(6, "Six");
        ones.put(7, "Seven");
        ones.put(8, "Eight");
        ones.put(9, "Nine");
    }

    public static String convertToWordCurrency(double value, String currencyCode) {
        StringBuilder inWords = new StringBuilder();
        Deque<String> words = convert(value, currencyCode);
        Iterator i = words.iterator();
        while (i.hasNext()) {
            inWords.append(i.next());
            inWords.append(" ");
        }
        return inWords.toString();
    }

    public static String convertToWordCurrency(double value) {
        return convertToWordCurrency(value, "");
    }

    private static Deque<String> convert(double value, String currencyCode) {
        Deque<String> words = new ArrayDeque<>();
        double remainder = 0;
        String decimalText = "";
        String fractionText = "";
        if(!"".equals(currencyCode)) {
            decimalText = currencyMap.get(currencyCode).get("decimalText");
            fractionText = currencyMap.get(currencyCode).get("fractionText");
        }

            // Converting fractional into word
//        String numberStr = Double.toString(value);
        String numberStr = String.format("%.2f", value);
        String fractionalStr = numberStr.substring(numberStr.indexOf('.') + 1);
        if (fractionalStr.length() > 2) fractionalStr = fractionalStr.substring(0, 2);
        if (fractionalStr.length() == 1) fractionalStr = fractionalStr + "0";

        int fractional = Integer.valueOf(fractionalStr);
        if (fractional > 0) {
            if(!"".equals(currencyCode)) {
                words.push(fractionText);
            }
            translateTees(words, fractional);
            words.push("and");
        }

        int lookup = (int) (value % Math.pow(dec, 2));
        words.push(decimalText);
        translateTees(words, lookup - remainder);
        remainder = lookup;

        //Hundreds
        lookup = (int) (value % Math.pow(dec, 3));
        if ((lookup - remainder) > 1)
            words.push(metricPosition.get(2));
        translateTees(words, (lookup - remainder) / Math.pow(dec, 2));
        remainder = lookup;

        //Thousand
        lookup = (int) (value % Math.pow(dec, 5));
        if ((lookup - remainder) > 1)
            words.push(metricPosition.get(3));
        translateTees(words, (lookup - remainder) / Math.pow(dec, 3));
        remainder = lookup;

        //Lakh
        lookup = (int) (value % Math.pow(dec, 7));
        if ((lookup - remainder) > 1)
            words.push(metricPosition.get(5));
        translateTees(words, (lookup - remainder) / Math.pow(dec, 5));
        remainder = lookup;

        //upto 10 crore
        lookup = (int) (value % Math.pow(dec, 9));
        if ((lookup - remainder) > 1)
            words.push(metricPosition.get(7));
        translateTees(words, (lookup - remainder) / Math.pow(dec, 7));
        //remainder = lookup;

        return words;
    }

    private static boolean translateTees(Deque<String> words, double value) {
        if (value == 0) {
            return false;
        }
        //handle teens
        if (value > 9 && value < 20) {
            words.push(teens.get((int) value));
            return true;
        }
        //format tees
        for (int i = 1; i < 3; i++) {
            double result = (value % Math.pow(dec, i)) / Math.pow(dec, i - 1);
            int lookup = (int) result;
            switch (i) {
                case 1:
                    if (lookup > 0)
                        words.push(ones.get(lookup));
                    break;
                case 2:
                    if (lookup > 1)
                        words.push(tees.get(lookup));
                    break;
            }
        }
        return true;
    }
}
