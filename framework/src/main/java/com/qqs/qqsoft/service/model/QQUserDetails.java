package com.qqs.qqsoft.service.model;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

public class QQUserDetails extends User {
    private Integer dbUserId;
    private String firstName;
    private String lastName;
    private String jwt;
    private Boolean active;

    public QQUserDetails(User user, Integer dbUserId, String firstName, String lastName) {
        super(user.getUsername(), user.getPassword(), user.getAuthorities());
        this.dbUserId = dbUserId;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public QQUserDetails(User user, Integer dbUserId, String firstName, String lastName, Boolean active) {
        super(user.getUsername(), user.getPassword(), user.getAuthorities());
        this.dbUserId = dbUserId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.active = active;
    }

    public QQUserDetails(String username, String password, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
    }

    public QQUserDetails(String username, String password, boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
    }

    public Integer getDbUserId() {
        return dbUserId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getJwt() {
        return jwt;
    }

    public void setJwt(String jwt) {
        this.jwt = jwt;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}
