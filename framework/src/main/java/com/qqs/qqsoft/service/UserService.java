package com.qqs.qqsoft.service;

import com.qqs.qqsoft.QQBusinessException;
import com.qqs.qqsoft.api.User;
import com.qqs.qqsoft.service.model.QQUserDetails;

import java.util.List;
import java.util.Map;

public interface UserService {
    QQUserDetails retrieveUser(String userName) throws QQBusinessException;

    QQUserDetails retrieveUserByToken(String token) throws QQBusinessException;

    com.qqs.qqsoft.api.User getUserById(Integer id) throws QQBusinessException;

    com.qqs.qqsoft.api.User getUserByuserName(String userName) throws QQBusinessException;

    //TODO : RAMA implement
    com.qqs.qqsoft.api.User saveUser(com.qqs.qqsoft.api.User user) throws QQBusinessException;

    List<User> searchUser(Map<String, String> params) throws QQBusinessException;

    Map<Integer, String> getUserNames() throws QQBusinessException;

}
