package com.qqs.qqsoft.service;

import com.qqs.qqsoft.QQBusinessException;
import com.qqs.qqsoft.service.model.QQUserDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Base64;

@Component
public class LoginService {
    Logger log = LoggerFactory.getLogger(LoginService.class);
    @Value("${com.qq.bypassSecurity:false}")
    private boolean internalTestOnly;

    @Resource(name = "userService")
    private UserService userService;

    BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(12);

    @Value("${com.qq.salt:sorna2025velu}")
    private String salt;

    @Value("${com.qq.separator:;}")
    private String separator;

    public QQUserDetails loginUser(String userName, String password) throws QQBusinessException {
        if (internalTestOnly)
            return createDummyUser();
        return loginThruDB(userName, password);
    }

    private QQUserDetails loginThruDB(String userName, String password) throws QQBusinessException {
        QQUserDetails authUser = userService.retrieveUser(userName);
        if (authUser == null) {
            throw new QQBusinessException("User auth failed");
        }
        log.debug("BCrypt : " + encoder.encode(authUser.getPassword()));
        if (!authUser.getActive() || !encoder.matches(password, authUser.getPassword())) {
            throw new QQBusinessException("User authentication failed");
        }
        return authUser;
    }

    public static String[] extractAndDecodeHeader(String header)
            throws IOException {
        byte[] base64Token = header.substring(6).getBytes("UTF-8");
        byte[] decoded;
        try {
            decoded = Base64.getDecoder().decode(base64Token);
        } catch (IllegalArgumentException e) {
            throw new BadCredentialsException(
                    "Failed to decode basic authentication token");
        }
        String token = new String(decoded, Charset.forName("UTF-8"));
        int delim = token.indexOf(":");
        if (delim == -1) {
            throw new BadCredentialsException("Invalid basic authentication token");
        }
        return new String[]{token.substring(0, delim), token.substring(delim + 1)};
    }

    public String encodePassword(String original){
        return encoder.encode(original);
    }

    private QQUserDetails createDummyUser() {
        UserDetails userDetails = User.withUsername("muruks")
                .password("dummy")
                .roles("ADMIN")
                .build();
        QQUserDetails authUser = new QQUserDetails((User)userDetails, 1, "Muruks", "Nambi");
        return authUser;
    }
}
