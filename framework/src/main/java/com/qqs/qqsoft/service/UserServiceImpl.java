package com.qqs.qqsoft.service;

import com.qqs.qqsoft.QQBusinessException;
import com.qqs.qqsoft.db.user.UserDataService;
import com.qqs.qqsoft.db.user.UserEntity;
import com.qqs.qqsoft.service.model.QQUserDetails;
import com.qqs.qqsoft.utils.ApiUtils;
import com.qqs.qqsoft.utils.DateUtils;
import com.qqs.qqsoft.utils.SearchCriteria;
import com.qqs.qqsoft.utils.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.*;


@Component("userService")
public class UserServiceImpl implements UserService {
    Logger log = LoggerFactory.getLogger(LoginService.class);

    @Resource
    SecurityUtils securityUtils;

    @Resource
    UserDataService userDataService;

    @Value("${com.qq.separator:;}")
    private String separator;

    //private Md5PasswordEncoder encoder = new Md5PasswordEncoder();

    @Value("${com.qq.salt:sorna2025velu}")
    private String salt;

    public static ApiUtils<com.qqs.qqsoft.api.User, UserEntity> userToDB = new ApiUtils<>();
    public static ApiUtils<UserEntity, com.qqs.qqsoft.api.User> userToAPI = new ApiUtils<>();
    public static ApiUtils<com.qqs.qqsoft.db.user.User, com.qqs.qqsoft.api.User> userIToAPI = new ApiUtils<>();

    @Override
    public QQUserDetails retrieveUser(String userName) throws QQBusinessException {
        Optional<UserEntity> userDB = userDataService.getUserByUserName(userName);
        if (!userDB.isPresent()) {
            throw new QQBusinessException("User auth failed");
        }
        UserEntity user = userDB.get();
        List<String> roles = new ArrayList<>();
        if (user.getRoles() != null) {
            for (String role : user.getRoles().split(separator)) {
                roles.add(role);
            }
        }
        UserDetails userDetails = User.withUsername(user.getUserName())
                .password(user.getPwd())
                .roles(roles.toArray(new String[0]))
                .build();
        QQUserDetails authUser = new QQUserDetails((User) userDetails, user.getId(), user.getFirstName(), user.getLastName(), user.getActive());
        return authUser;
    }

    @Override
    public QQUserDetails retrieveUserByToken(String token) throws QQBusinessException {
        throw new QQBusinessException("Not implemented");
    }


    public Map<Integer, String> getUserNames() {
        Map<Integer, String> userMap = new HashMap<>();
        Optional<List<Object[]>> userList = userDataService.getAllUserName();
        userList.ifPresent(objects -> objects.forEach(item -> userMap.put(((Integer) item[0]), ((String) item[1]))));
        return userMap;
    }


    @Override
    public com.qqs.qqsoft.api.User getUserById(Integer id) throws QQBusinessException {
        com.qqs.qqsoft.api.User selectedUser = null;
        try {
            Optional<UserEntity> user = userDataService.getUserById(id);
            if (user.isPresent()) {
                selectedUser = userToAPI.translate(user.get(), com.qqs.qqsoft.api.User.class, true);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return selectedUser;
    }


    @Override
    public com.qqs.qqsoft.api.User getUserByuserName(String userName) throws QQBusinessException {
        com.qqs.qqsoft.api.User userToApi = null;
        try {
            Optional<com.qqs.qqsoft.db.user.User> user = userDataService.getUserByuserName(userName);
            userToApi = userIToAPI.translate(user.get(), com.qqs.qqsoft.api.User.class, false);
            if (user.isPresent()) {
                return userToApi;
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }

    //TODO : RAMA implement
    @Override
    public com.qqs.qqsoft.api.User saveUser(com.qqs.qqsoft.api.User user) throws QQBusinessException {
        com.qqs.qqsoft.api.User userToApi = null;
        Integer loggedInUser = securityUtils.getLoggedInUser();

        try {
            UserEntity toSaveUser =  userToDB.translate(user, UserEntity.class, true);
            if(toSaveUser.getId() > 0) {
                new DateUtils<UserEntity>().setTimeStamp(toSaveUser, UserEntity.class, true);
                toSaveUser.setModifiedBy(loggedInUser);
            } else {
                new DateUtils<UserEntity>().setTimeStamp(toSaveUser, UserEntity.class, false);
                toSaveUser.setCreatedBy(loggedInUser);
                // toSaveUser.setPwd(encoder.encodePassword(toSaveUser.getPwd(), salt));
            }
            UserEntity savedUser = userDataService.saveUser(toSaveUser);
            userToApi = userToAPI.translate(savedUser , com.qqs.qqsoft.api.User.class, true);

        } catch (Exception e ) {
            log.error(e.toString());
            throw new QQBusinessException("Error while saving User");
        }
        return userToApi;
    }

    @Override
    public List<com.qqs.qqsoft.api.User> searchUser(Map<String, String> params) {
        List<com.qqs.qqsoft.api.User> users = new ArrayList<>();
        if (params.get("userName") != null) {
            Optional<List<UserEntity>> userList = userDataService.findAllByUserNameIsLike(params.get("userName"));
            try {
                users = userToAPI.translate(userList.get() , com.qqs.qqsoft.api.User.class, true);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            Iterable<UserEntity> userList = userDataService.findAll();
            try {
                users = userToAPI.translate(userList, com.qqs.qqsoft.api.User.class, true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return users;
    }

}
