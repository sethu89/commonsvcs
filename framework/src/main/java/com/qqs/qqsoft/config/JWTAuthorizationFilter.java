package com.qqs.qqsoft.config;

import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.qqs.qqsoft.service.model.QQUserDetails;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.annotation.Resource;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component("jwtAuthFilter")
public class JWTAuthorizationFilter extends OncePerRequestFilter {
    private static final String AUTH_HEADER = "Authorization";
    private static final String TOKEN_PREFIX = "Bearer ";
    Logger log = LoggerFactory.getLogger(JWTAuthorizationFilter.class);

    @Resource(name = "jwtQQSDecoder")
    private JWTVerifier decoder;

    public JWTAuthorizationFilter() {
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {

        //log.info("*************** AUTH Filter executed **************");
        UsernamePasswordAuthenticationToken authentication;
        String header = request.getHeader(AUTH_HEADER);
        if (header == null || !header.startsWith(TOKEN_PREFIX)) {
            authentication = getDummyAuth();
        } else {
            authentication = getAuthentication(request);
        }
        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(request, response);
    }

    private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request) {
        try {
            String token = request.getHeader(AUTH_HEADER);
            token = token.replace(TOKEN_PREFIX, "");
            if (token != null) {
                DecodedJWT jwt = decoder.verify(token);
                QQUserDetails user = parseJWT(jwt);
                if (user != null) {
                    return new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());
                }
                return null;
            }
        } catch (Exception e) {
            log.error("****** Auth failed :: " + e.getMessage());
        }
        return null;
    }

    private QQUserDetails parseJWT(DecodedJWT jwt) {
        String[] roles = StringUtils.split(jwt.getClaim("roles").asString().replace("ROLE_", ""), '|');
        String[] name = StringUtils.split(jwt.getClaim("name").asString(), ", ");
        Integer dbUserId = jwt.getClaim("user").asInt();

        UserDetails user = User.builder()
                .username(dbUserId.toString())
                .password("none")
                .roles(roles)
                .build();
        QQUserDetails qqUser = new QQUserDetails((User) user, dbUserId, name[1], name[0]);
        qqUser.setJwt(jwt.getToken());

        return qqUser;
    }

    private UsernamePasswordAuthenticationToken getDummyAuth(){
        UserDetails user = User.builder()
                .username("0")
                .password("none")
                .roles("NONE")
                .build();
        QQUserDetails qqUser = new QQUserDetails((User) user, 0, "No", "Name");
        return new UsernamePasswordAuthenticationToken(qqUser, null, qqUser.getAuthorities());
    }
}
