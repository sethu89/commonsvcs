package com.qqs.qqsoft.config;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.RSAKeyProvider;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

@Configuration
public class AppConfig {

    @Value("${key.location:qqs-pub.crt}")
    private String certPath;

    @Bean("jwtQQSDecoder")
    public JWTVerifier jwtDecoder(ApplicationContext ctx) throws CertificateException, IOException {
        org.springframework.core.io.Resource resource = ctx.getResource(certPath);
        CertificateFactory factory = CertificateFactory.getInstance("X.509");
        Certificate cert = factory.generateCertificate(resource.getInputStream());
        RSAPublicKey key = (RSAPublicKey) cert.getPublicKey();
        JWTVerifier jwt = JWT.require(Algorithm.RSA256(new RSAKeyProvider() {
            @Override
            public RSAPublicKey getPublicKeyById(String keyId) {
                return key;
            }

            @Override
            public RSAPrivateKey getPrivateKey() {
                return null;
            }

            @Override
            public String getPrivateKeyId() {
                return null;
            }
        }))
                .build();
        return jwt;
    }
}
