package com.qqs.qqsoft.service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.qqs.qqsoft.service.model.QQUserDetails;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;

@Component
public class JWTProvider {

    Logger log = LoggerFactory.getLogger(JWTProvider.class);

    @Value("${jwt.expiration.secs:3600}0")
    Integer tokenExpiration;

    @Value("${public.key.location}")
    String publicKeyLocation;
    @Value("${private.key.location}")
    String privateKeyLocation;

    public String createAuthToken(QQUserDetails user) throws CertificateException, IOException, InvalidKeySpecException, NoSuchAlgorithmException {
        RSAPublicKey publicKey = getPublicKey();
        RSAPrivateKey privateKey = getPrivateKey();
        try {
            Algorithm algorithm = Algorithm.RSA256(publicKey, privateKey);
            List<String> roles = new ArrayList<>();
            user.getAuthorities().forEach(role ->{
                roles.add(role.getAuthority());
            });
            Date currentTime = new Date(System.currentTimeMillis());
            String token = JWT.create()
                    .withIssuer("QandQ Solutions")
                    .withClaim("user", user.getDbUserId())
                    .withClaim("name", user.getLastName() + ", " + user.getFirstName())
                    .withClaim("roles", StringUtils.join(user.getAuthorities(), '|'))
                    .withArrayClaim("roleArray", roles.toArray(new String[]{}))
                    .withIssuedAt(currentTime)
                    .withExpiresAt(DateUtils.addSeconds(currentTime, tokenExpiration))
                    .sign(algorithm);
            return token;
        } catch (JWTCreationException e) {
            log.error("JWT Creation failed " + e.getMessage());
        }
        return null;
    }

    private RSAPrivateKey getPrivateKey() throws CertificateException, IOException, InvalidKeySpecException, NoSuchAlgorithmException {
        org.springframework.core.io.Resource resource = new FileSystemResource(privateKeyLocation);
        String privateKeyPEM = new String(resource.getInputStream().readAllBytes());
        // strip of header, footer, newlines, whitespaces
        privateKeyPEM = privateKeyPEM
                .replace("-----BEGIN PRIVATE KEY-----", "")
                .replace("-----END PRIVATE KEY-----", "")
                .replaceAll("\\s", "");
        // decode to get the binary DER representation
        byte[] privateKeyDER = Base64.getDecoder().decode(privateKeyPEM);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        RSAPrivateKey privateKey = (RSAPrivateKey) keyFactory.generatePrivate(new PKCS8EncodedKeySpec(privateKeyDER));
        return privateKey;
    }

    private RSAPublicKey getPublicKey() throws CertificateException, IOException {
        org.springframework.core.io.Resource resource = new FileSystemResource(publicKeyLocation);
        CertificateFactory factory = CertificateFactory.getInstance("X.509");
        Certificate cert = factory.generateCertificate(resource.getInputStream());
        RSAPublicKey key = (RSAPublicKey) cert.getPublicKey();
        return key;
    }

}
