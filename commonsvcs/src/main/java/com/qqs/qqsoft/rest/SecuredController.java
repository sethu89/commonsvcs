package com.qqs.qqsoft.rest;

import com.qqs.qqsoft.QQBusinessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 *
 * Sample secured controller.
 */

@CrossOrigin(origins = "*", allowCredentials = "true", allowedHeaders = "*")
@RestController
@RequestMapping(value = "/codes")
public class SecuredController {

    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MASTER_WRITE','ROLE_ALL_WRITE')")
    @RequestMapping(method = RequestMethod.GET, value = "/try", produces = "application/json")
    public ResponseEntity<String> saveCodes(HttpServletRequest request) throws QQBusinessException {
        ResponseEntity<String> result = new ResponseEntity("Success", HttpStatus.OK);
        return result;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/open", produces = "application/json")
    public ResponseEntity<String> openRequest(HttpServletRequest request) throws QQBusinessException {
        ResponseEntity<String> result = new ResponseEntity("Success", HttpStatus.OK);
        return result;
    }

}
