package com.qqs.qqsoft.rest;


import com.qqs.qqsoft.QQBusinessException;
import com.qqs.qqsoft.api.Credential;
import com.qqs.qqsoft.api.User;
import com.qqs.qqsoft.service.JWTProvider;
import com.qqs.qqsoft.service.LoginService;
import com.qqs.qqsoft.service.UserService;
import com.qqs.qqsoft.service.model.QQUserDetails;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.security.cert.CertificateException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping(value = "/access")
public class UserController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @Resource
    UserService userService;

    @Resource
    private LoginService loginService;

    @Value("${app.response.jwt.token:X-QQ-Auth-token}")
    private String authToken;

    @Value("${app.response.user.roles:X-QQ-Auth-roles}")
    private String rolesHeader;

    @Resource
    JWTProvider jwtProvider;

    @RequestMapping(method = RequestMethod.GET, value = "/login", produces = "application/json")
    public ResponseEntity<String> login(HttpServletRequest request) throws QQBusinessException {
        ResponseEntity<String> result;
        try {
            String header = request.getHeader("Authorization");
            if(header == null || header.isBlank()){
                return new ResponseEntity<>("Auth failure", null, HttpStatus.FORBIDDEN);
            }
            String[] userInfo = LoginService.extractAndDecodeHeader(header);
            QQUserDetails user = loginService.loginUser(userInfo[0], userInfo[1]);
            MultiValueMap<String, String> responseHeaders = new HttpHeaders();
            responseHeaders.add(authToken, jwtProvider.createAuthToken(user));
            responseHeaders.add(rolesHeader, StringUtils.join(user.getAuthorities(), '|'));
            result = new ResponseEntity(user.getDbUserId() , responseHeaders, HttpStatus.OK);
        } catch (IOException e) {
            LOGGER.error("Login Failed ", e);
            result =  new ResponseEntity("Login Failed", HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (CertificateException e) {
            LOGGER.error("Login Failed - Cert error ", e);
            result =  new ResponseEntity("Login Failed - Cert error", HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception e) {
            LOGGER.error("Login Failed - Other Exception ", e);
            result =  new ResponseEntity("Login Failed - Server error", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return result;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/logout", produces = "application/json")
    public ResponseEntity<String> logout(HttpServletRequest request) throws QQBusinessException {
        new SecurityContextLogoutHandler().logout(request, null, null);
        ResponseEntity<String> result = new ResponseEntity("Logged out", HttpStatus.OK);
        return result;
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER', 'ROLE_OPERATOR')")
    @RequestMapping(method = RequestMethod.POST, value = "/password/reset", produces = "application/json")
    public ResponseEntity<String> resetPassword(@RequestBody Credential form) throws QQBusinessException {
        User dbUser = userService.getUserByuserName(form.getUserName());
        if(dbUser == null){
            throw new QQBusinessException("User not valid");
        }
        QQUserDetails validLogin = loginService.loginUser(form.getUserName(), form.getCurrentPwd());
        if(validLogin == null){
            throw new QQBusinessException("User not valid");
        }
        String encrypted = loginService.encodePassword(form.getNewPwd());
        dbUser.setPwd(encrypted);
        userService.saveUser(dbUser);
        ResponseEntity<String> result = new ResponseEntity("OK", HttpStatus.OK);
        return result;
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER', 'ROLE_OPERATOR')")
    @RequestMapping(method = RequestMethod.GET, value = "/searchUser", produces = "application/json")
    public ResponseEntity<List<User>> searchUser(@RequestParam Map<String, String> params,
                                                 HttpServletRequest request) throws QQBusinessException {
        List<User> saved = userService.searchUser(params);
        ResponseEntity<List<User>> result = new ResponseEntity(saved, HttpStatus.OK);
        return result;
    }
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER', 'ROLE_OPERATOR')")
    @RequestMapping(method = RequestMethod.GET, value = "/searchUser/byId", produces = "application/json")
    public ResponseEntity<User> getUserById(@RequestParam Integer id,
                                            HttpServletRequest request) throws QQBusinessException {
        User user = userService.getUserById(id);
        ResponseEntity<User> result = new ResponseEntity(user, HttpStatus.OK);
        return result;
    }

    // TODO : Rama - figure out why do we need this
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER', 'ROLE_OPERATOR')")
    @RequestMapping(method = RequestMethod.GET, value = "/getUserName", produces = "application/json")
    public ResponseEntity<Map<Integer, String>> getUserNames() throws QQBusinessException {
        Map<Integer, String > userNames = userService.getUserNames();
        ResponseEntity<Map<Integer, String>> result = new ResponseEntity(userNames, HttpStatus.OK);
        return result;
    }


    // GET USER
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER', 'ROLE_OPERATOR')")
    @RequestMapping(method = RequestMethod.GET, value = "/getUserDetail/byUserName", produces = "application/json")
    public ResponseEntity<User> retrieveUser(@RequestParam String userName) throws QQBusinessException {
        User userNames = userService.getUserByuserName(userName);
        ResponseEntity<User> result = new ResponseEntity(userNames, HttpStatus.OK);
        return result;
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    @RequestMapping(method = RequestMethod.POST, value = "/saveUser", produces = "application/json")
    public ResponseEntity<User> saveUser(@RequestBody User form) throws QQBusinessException {
        String encrypted = loginService.encodePassword(form.getPwd());
        form.setPwd(encrypted);
        User saved = userService.saveUser(form);
        ResponseEntity<User> result = new ResponseEntity(saved, HttpStatus.OK);
        return result;
    }
}
